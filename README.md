# gtao-ocr
## OCR for Grand Theft Auto Online player lists


### Running examples

To run the test/example scripts you will need some sample player list screen shots from GTA:O.

1. Copy one or more PNG files to samples/#.png where # is from 1 to n (number of samples).
2. Create samples/samples.txt, each line is a space separated list of actual user names in the corresponding sample PNG, ie. line 1 corresponds to 1.png.


#### ocr_tuning.py

This script will run OCR against each sample and compare against the actual values in samples.txt.  It will output accuracy per sample and overall accuracy, allowing for comparisions between OCR/image processing parameters.


#### ocr_auto_learn_test.py

This script will run ocr_auto_learn against the samples by first loading the actual values from samples.txt into the database.  The output (per sample) is a count of exact matches, fuzzy matches and a list of user names that could not be matched.
