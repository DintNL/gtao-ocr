import cv2

def grayscale(image):
    """Convert image to grayscale."""
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

def invert(image):
    """Invert image colors (or tones for grayscale)."""
    return cv2.bitwise_not(image)

def scale(image, multiplier=3, interpolation=cv2.INTER_CUBIC):
    """Uniformly scale image by a multiplier."""
    return cv2.resize(image, (int(image.shape[1] * multiplier), int(image.shape[0] * multiplier)), interpolation=interpolation)

def threshold(image, lower=127):
    """Apply a binary threshold to image."""
    thresh = cv2.threshold(image, lower, 255, cv2.THRESH_BINARY)
    return thresh[1]

def blobfill(image, multiplier=3):
    """Fill large blobs within image with white."""
    contours, _ = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # skip first contour, detects whole image
    for contour in contours[1:]:
        approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        x, y, w, h = cv2.boundingRect(approx)
        # KLUDGE fill large regions only, there's probaby a better way of filtering
        if w * h > 2400 * multiplier:
            cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 255), -1)

    return image

def opening(image):
    """Erode and dialate image."""
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (1, 2))
    return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
